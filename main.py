#!/usr/bin/env python
# -*- coding: utf-8 -*-


from uuid import uuid4

from telegram.utils.helpers import escape_markdown

import config

from telegram import InlineQueryResultArticle, ParseMode, \
    InputTextMessageContent
from telegram.ext import Updater, InlineQueryHandler, CommandHandler
import logging

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')


def inlinequery(bot, update):
    """Handle the inline query."""
    query = update.inline_query.query
    results = [
        InlineQueryResultArticle(
            id=uuid4(),
            title="Diskussion",
            input_message_content=InputTextMessageContent(
                "@fffdiskussion")),
        InlineQueryResultArticle(
            id=uuid4(),
            title="Netiquette",
            input_message_content=InputTextMessageContent(
                "*Ihr findet die Netiquette unter* fffutu.re/netiquette! Wer sich nicht daran hält wird bestraft!",
                parse_mode=ParseMode.MARKDOWN)),
        InlineQueryResultArticle(
            id=uuid4(),
            title="Caps-Lock",
            input_message_content=InputTextMessageContent(
                "*Bitte schalte dein CAPS-lock aus!*",
                parse_mode=ParseMode.MARKDOWN)),
        InlineQueryResultArticle(
            id=uuid4(),
            title="Gruppen",
            input_message_content=InputTextMessageContent(
                "*Alle deutschlandweite FFF Gruppen sind hier gelistet:*\n t.me/FridaysForFutureDE\nt.me/FFFDiskussion\nt.me/FFFMobilitaet\nt.me/FFFErnaehrung\nt.me/FFFEnergie\nt.me/FFFVerpackungen\nt.me/FFFKommunikation\nt.me/FFFSozialeFrage\nt.me/FFFGesellschaft\nt.me/FFFPolitik\nt.me/FFFWissenschaft\nt.me/FFFSonstiges\nt.me/FFFOffTopic\n",
                disable_web_page_preview=True,
                parse_mode=ParseMode.MARKDOWN)),
        InlineQueryResultArticle(
            id=uuid4(),
            title="No Spam",
            input_message_content=InputTextMessageContent(
                "Bitte hör auf zu spammen! Sonst wirst du *gemutet oder gebannt!*",
                parse_mode=ParseMode.MARKDOWN))]

    update.inline_query.answer(results)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater(config.token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(InlineQueryHandler(inlinequery))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
